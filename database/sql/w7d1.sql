DROP TABLE IF EXISTS `countries`;
DROP TABLE IF EXISTS `job_history`;
DROP TABLE IF EXISTS `jobs`;
DROP TABLE IF EXISTS `employees`;
DROP TABLE IF EXISTS `car`;
DROP TABLE IF EXISTS `lot`;
DROP TABLE IF EXISTS `departments`;
DROP TABLE IF EXISTS `employee`;

-- country_id, country_name and region_id

CREATE TABLE countries (

	country_id VARCHAR(64),
    country_name VARCHAR(64),
    region_id VARCHAR(64)
	
);

-- job_id, job_title, min_salary, max_salary

CREATE TABLE jobs (
	job_id VARCHAR(64),
    job_title VARCHAR(64),
    min_salary NUMERIC(12, 2),
    max_salary NUMERIC(12 , 2),
    
    CONSTRAINT job_id PRIMARY KEY(job_id)
);

-- employee_id, start_date, end_date, job_id and department_id

/*
department_id DECIMAL(4,0) (Primary Key)
department_name VARCHAR(30)
manager_id DECIMAL(6,0) -- Default to 0 (Primary Key)
locaton_id DECIMAL(4,0) -- Default to null
*/

CREATE TABLE departments (
	department_id DECIMAL(4,0) UNIQUE,
	department_name VARCHAR(30) UNIQUE NOT NULL,
	manager_id DECIMAL(6,0) UNIQUE NOT NULL DEFAULT 0, -- Default to 0
	locaton_id DECIMAL(4,0) DEFAULT NULL, -- Default to null
    
    PRIMARY KEY(manager_id, department_id)
);

/*
employee_id 
first_name 
last_name 
email 
phone_number
hire_date 
job_id 
salary 
commission 
manager_id 
department_id
*/

CREATE TABLE employees (
	employee_id VARCHAR(64) UNIQUE,
	first_name  VARCHAR(64),
	last_name VARCHAR(64),
	email VARCHAR(256),
	phone_number CHAR(10),
	hire_date TIMESTAMP,
	job_id VARCHAR(64),
	salary DECIMAL(12,2),
	commission DECIMAL(10,2),
	manager_id DECIMAL(6,0),
	department_id DECIMAL (4,0),
    
    CONSTRAINT employee_id PRIMARY KEY(employee_id),
    CONSTRAINT manager_id_department_id_fk FOREIGN KEY(manager_id, department_id) REFERENCES departments(manager_id, department_id)
);

CREATE TABLE job_history (
	employee_id VARCHAR(64),
    start_date TIMESTAMP,
    end_date TIMESTAMP,
    job_id VARCHAR(64),
    department_id DECIMAL (4,0),
    CONSTRAINT department_id FOREIGN KEY(department_id) REFERENCES departments(department_id),
    CONSTRAINT job_id_fk FOREIGN KEY(job_id) REFERENCES jobs(job_id),
    CONSTRAINT employee_id_fk FOREIGN KEY(employee_id) REFERENCES employees(employee_id)
);

CREATE TABLE employee (
	employee_id INTEGER UNIQUE AUTO_INCREMENT,
    `name` VARCHAR(128),
    start_date TIMESTAMP,
    term_date TIMESTAMP,
    username VARCHAR(128),
    birthdate TIMESTAMP,
    `status` VARCHAR(10),
    
    PRIMARY KEY(employee_id)
);

CREATE TABLE lot(
	lot_id INTEGER AUTO_INCREMENT,
    address VARCHAR(1024),
    manager_id INTEGER,
    
    PRIMARY KEY(lot_id),
    CONSTRAINT manager_id_fk FOREIGN KEY(manager_id) REFERENCES employee(employee_id)
);

CREATE TABLE car (
	car_id INTEGER AUTO_INCREMENT,
    car_make VARCHAR(64),
    car_model VARCHAR(64),
    car_year INTEGER,
    car_mile INTEGER,
    car_image LONGBLOB,
    lot_id INTEGER,
    
    PRIMARY KEY(car_id),
    CONSTRAINT lot_id_fk FOREIGN KEY(lot_id) REFERENCES lot(lot_id)    
);