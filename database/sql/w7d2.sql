-- DROP TABLE car;
-- DROP TABLE employee;

INSERT INTO employee(`name`, start_date, term_date, username, birthdate, `status`)
VALUES ("FRANK", "1992-07-25", null, null, null, 'ACTIVE'),
("SUSAN", "2002-05-01", null, null, null, 'ACTIVE'),
("JOHN", "1998-02-12", "2005-08-23", null, null, 'INACTIVE'),
("ALLEN", "2010-10-31",null, null, null, 'ACTIVE');

INSERT INTO lot(address, manager_id)
VALUES
("123 Fake Street", (SELECT employee_id FROM employee WHERE `name`='Frank')),
("394 Fun Street", (SELECT employee_id FROM employee WHERE `name`='Susan'));

-- car_id VARCHAR(64),
--     car_make VARCHAR(64),
--     car_model VARCHAR(64),
--     car_year INTEGER,
--     car_milage INTEGER,
--     car_image LONGBLOB,
--     lot_id INTEGER,

INSERT INTO car(car_make, car_model, car_year, car_mile, car_image, lot_id)
VALUES
("Toyota", "Corolla", 2008, 100000, null, (SELECT lot_id FROM lot WHERE address='123 Fake Street')),
("Mazda", "3", 2015, 45000, null, (SELECT lot_id FROM lot WHERE address='123 Fake Street')),
("Ford", "Taurus", 2010, 125000, null, (SELECT lot_id FROM lot WHERE address='123 Fake Street')),
("Chrysler", "PT Cruiser", 2008, 88000, null, (SELECT lot_id FROM lot WHERE address='394 Fun Street')),
("BMW", "M3", 2011, 95000, null, (SELECT lot_id FROM lot WHERE address='394 Fun Street')),
("Toyota", "Camry", 1999, 200000, null, (SELECT lot_id FROM lot WHERE address='394 Fun Street'));

SELECT *
FROM employee
WHERE `status` = 'Active';

SELECT *
FROM employee
WHERE start_date < '2000-01-01';

SELECT *
FROM car
WHERE car_mile < 200000;

SELECT *
FROM car
WHERE car_year > 2005
AND car_mile > 80000;

/*
Select all the active employees.
Select employees who started before the year 2000.
Select all Toyota cars that have less than 200000 miles.
Select all vehicles made after 2005 with more than 80000 miles.

*/